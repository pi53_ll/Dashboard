function openLeftMenu() {
    document.getElementById("container").style.marginLeft= "21%";
    document.getElementById("leftMenu").style.display="block";
    document.getElementById("openLeft").style.display = 'none';
}

function closeLeftMenu() {
    document.getElementById("container").style.marginLeft= "1%";
    document.getElementById("leftMenu").style.display='none';
    document.getElementById("openLeft").style.display="inline-block";
}

function openRightMenu() {
    document.getElementById("rightMenu").style.display="block";
    document.getElementById("openRight").style.display="inline-block";
}

function closeRightMenu() {
    document.getElementById("rightMenu").style.display='none';
    document.getElementById("openRight").style.display="inline-block";
}